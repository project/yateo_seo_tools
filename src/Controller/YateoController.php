<?php
/**
 * @file
 * Contains \Drupal\yateo\Controller\HelloController.
 */
namespace Drupal\Yateo\Controller;

class YateoController {

	public function content() {
		global $base_url;

		$html = '<h3>SEO TOOLS</h3>	';

		$html .= '<div class="col-xs-12">';

		$html .= '<p><a target="_blank" href="https://fr.semrush.com/analytics/organic/overview/?db=fr&q='.$base_url.'/&searchType=url">Semrush</a></p>';
		$html .= '<p><a target="_blank" href="https://ahrefs.com/site-explorer/overview/v2/subdomains/live?target='.$base_url.'%2F">Ahrefs</a></p>';
		$html .= '<p><a target="_blank" href="https://fr.majestic.com/reports/site-explorer?q='.$base_url.'&oq=url.delapage&IndexDataSource=F">Majestiv SEO</a></p>';
		$html .= '<p><a target="_blank" href="https://search.google.com/structured-data/testing-tool#url='.$base_url.'">Google structured data</a></p>';
		$html .= '<p><a target="_blank" href="https://developers.google.com/speed/pagespeed/insights/?url='.$base_url.'">Google PageSpeed</a></p>';

		$html .= '</div>';

		$array = array(
			'#type' => "markup",
			'#markup' => t($html),
		);

		return $array;
	}
	
}
